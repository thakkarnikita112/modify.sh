#!/bin/sh 

# Task 1

#Help Command
if [ $1 = "-h" ];then
cat <<END
Do any one of the following:
	./modify.sh -l|-u <dir/file names...>
	./modify.sh sed <pattern> <dir/file names...>
	./modify.sh -h
END
# exit after running the the help text
exit 0
fi

# assign f and s as temporary variable that for any value to replace
# shift operation while argument is doing
f=n
s=n

# if -l is given by the user
# use for change the file name uppercase to lowercase
if [ $1 = "-l" ]; then
# file exits or not
# if exits
	if [ ! -f "$2" ] ; then
		`echo "file $2 does not exists" 1>&2`
	else
# shift arguments is use for change them to uppercase to lowercase one by one
		while test "x$2" != "x"
		do
			case "$1" in
                	-f|--first) f=y;;
                	-s|--second) s=y;;
                	-w) with_arg "$3"; shift;;
        	esac
		for i in $( echo $2 );
#uppercase to lowercase
			do mv -i $i `echo $i | tr [:upper:] [:lower:]`; #tracing Dir/filename characters and moving from uppercase to lowercase
		done
        	shift
		done
	echo "Changes from Lowercase to uppercase has been made."	
	fi

# if -u is given by the user then it changes lowercase to uppercase
elif [ $1 = "-u" ]; then
# file exits or not?
# if exits
	if [ ! -f "$2" ] ; then
		`echo "file $2 does not exists" 1>&2`
# shift arguments is use for change them to lowercase to uppercase one by one
	else
		while test "x$2" != "x"
		do
			case "$1" in
                	-f|--first) f=y;;
                	-s|--second) s=y;;
                	-w) with_arg "$3"; shift;;
        	esac
		for i in $( echo $2 );
#lowercase to uppercase
			do mv -i $i `echo $i | tr [:lower:] [:upper:]`; #tracing Dir/filename characters and moving from lowercase to uppercase
		done
        	shift
		done
	echo "Changes from uppercase to lowercase has been made."
	fi

# sed pattern
# change file name according to  sed 
elif [ $1 = sed ]; then
	k=$(echo "$3" | $1 $2)
	mv -i $3 "$k"
fi


