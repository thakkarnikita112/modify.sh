#include <stdio.h>/*is a statement which tells the compiler to insert the contents of stdio at that particular place*/
#include <signal.h>/*defines a variable type sig_atomic_t, two function calls, and several macros to handle different signals reported during a program's execution*/
#include <wait.h>/*declarations for waiting*/
#include <stdlib.h>/* defines four variable types, several macros, and various functions for performing general functions*/
#include <unistd.h>/* header defines miscellaneous symbolic constants and types, and declares miscellaneous functions*/


#define NUM_CHILD 13
#define WITH_SIGNALS

#ifdef WITH_SIGNALS
	char interrupt_flag = 0;
	/*
	->signal from the keyboard as the interrupt/
	->the interrupt occurance is marked/
	*/
	void keyboardInterrupt(int ya) {
		printf("parent [%i]: The keyboard interrupt received.\n",getpid());
		interrupt_flag = 1;
	}
	void killedChild(int ha) {
		printf("child [%i]: Termination of the process.", getpid());
	}
#endif

int main(){
	pid_t child_pid=1;		// temp child process PID
	pid_t parent_pid=1;		// temp parent process PID
	int i,j,cnt=0;			// temp count vars
	int w,s;			// process status		
	
	printf("parent[%i]: The parent is starting.\n",getpid());
	for (i = 0; i < NUM_CHILD; ++i) {
		parent_pid=getpid();
		/*
		Force ignoring of all signals with the signal() (or sigaction()) but
		after that at once restore the default handler for SIGCHLD signal
		*/
		#ifdef WITH_SIGNALS
			for(j = 0; j < NSIG; ++j){ // NSIG - the total number of signals defined
				signal(SIGINT,SIG_IGN);
			}
			signal (SIGCHLD,SIG_DFL);
			/*
			Set your own keyboard interrupt signal handler (symbol of this interrupt:  SIGINT)
			*/
			signal (SIGINT,keyboardInterrupt);
		#endif
		if (!(child_pid = fork())){
			/*
			In the child process
			  a. set to ignore handling of the keyboard interrupt signal
			*/
			#ifdef WITH_SIGNALS
				signal (SIGINT,killedChild);
				signal (SIGINT,SIG_DFL);
			#endif
			printf("child [%i]: I'm created.\n",getpid());
			sleep(10);
			printf("child [%i]: I've completed execution.\n",getpid());
			exit(0);
			}
		else if (child_pid == -1){
			printf("parent [%i]: Couldn't create new child.\n",getpid());
			kill(-1,SIGTERM);
			exit(1);
		}
		sleep(1);
		
		/*
		Between the two consequtive creations of new processes 
		check the mark which may be set by the keyboard interrupt handler. 
		If the mark is set the parent process should signal all just 
		created processes with the SIGTERM and, instead of printing the 
		message about creation, print out a message about interrupt of 
		the creation process. After that, the process should continue with wait()'s loop as before.
		*/
		#ifdef WITH_SIGNALS
			if (interrupt_flag){
				printf("parent [%i]: Interrupt of the creation process!\n", getpid());
				kill(-2,SIGTERM);
				break;
			}
		#endif
	}
	
	while(1){
		w = wait(&s);
		if(w == -1)
			break;
		else{
			printf("child [%i]: I've finished.\n",w);
			cnt++;
		}
	}
	printf("\nSuccess: %d processes were terminated.\n", cnt);
	/*
	 At the very end of the main process, the old service handlers of all
	 signals should be restored.
	*/
	#ifdef WITH_SIGNALS
		for(j=0; j<NSIG;j++)
			signal(j,SIG_DFL);
	#endif
	return 0;
}
	
